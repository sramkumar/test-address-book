#!/bin/bash
echo "============Installing Project=============="

echo "=============COMPOSER INSTALL============="
composer install

echo "=============RUN MIGRATIONS============="
php bin/console doctrine:migrations:migrate  --no-interaction

echo "=============CACHE CLEAR============="
php bin/console cache:clear


echo "=============ASSETIC Install============="
php bin/console assets:install --symlink

echo "====================UNIT Testing==============="
./bin/phpunit
