<?php

namespace App\Controller;

use App\Entity\AddressBook;
use App\Form\AddressBookType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class AddressBookController extends Controller
{
    /**
     * @Route("/", name="address_book_index", methods={"GET"})
     */
    public function index(): Response
    {
        $addressBooks = $this->getDoctrine()
            ->getRepository(AddressBook::class)
            ->findAll();

        return $this->render('address_book/index.html.twig', [
            'address_books' => $addressBooks,
        ]);
    }

    /**
     * @Route("/new", name="address_book_new", methods={"GET","POST"})
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $addressBook = new AddressBook();
        $form = $this->createForm(AddressBookType::class, $addressBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            /** @var UploadedFile $file */
            $file = $form['file']->getData();
            if ($file) {
                $fileName = $fileUploader->upload($file);
                $addressBook->setPicture($fileName);
            }
            $entityManager->persist($addressBook);
            $entityManager->flush();

            return $this->redirectToRoute('address_book_index');
        }

        return $this->render('address_book/new.html.twig', [
            'address_book' => $addressBook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="address_book_show", methods={"GET"})
     * @param AddressBook $addressBook
     * @return Response
     */
    public function show(AddressBook $addressBook): Response
    {
        return $this->render('address_book/show.html.twig', [
            'address_book' => $addressBook,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="address_book_edit", methods={"GET","POST"})
     * @param Request $request
     * @param AddressBook $addressBook
     * @return Response
     */
    public function edit(Request $request, AddressBook $addressBook): Response
    {
        $form = $this->createForm(AddressBookType::class, $addressBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('address_book_index');
        }

        return $this->render('address_book/edit.html.twig', [
            'address_book' => $addressBook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="address_book_delete", methods={"DELETE"})
     * @param Request $request
     * @param AddressBook $addressBook
     * @return Response
     */
    public function delete(Request $request, AddressBook $addressBook): Response
    {
        if ($this->isCsrfTokenValid('delete'.$addressBook->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($addressBook);
            $entityManager->flush();
        }

        return $this->redirectToRoute('address_book_index');
    }
}
