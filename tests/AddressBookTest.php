<?php

namespace App\Tests;

use App\Entity\AddressBook;
use App\Form\AddressBookType;
use Symfony\Component\Form\Test\TypeTestCase;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Validator\Validation;

class AddressBookTest extends TypeTestCase
{
    private $objectManager;

    protected function setUp()
    {
        // mock any dependencies
        $this->objectManager = $this->createMock(ObjectManager::class);

        parent::setUp();
    }

    protected function getExtensions()
    {
        $type = new AddressBook($this->objectManager);

        $validator = Validation::createValidator();

        // or if you also need to read constraints from annotations
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();


        return [
            new PreloadedExtension([$type], []),
            new ValidatorExtension($validator),
        ];
    }

    public function testSubmitInvalidData()
    {
        $formData = [
            'firstName' => '',
            'lastName' => 'test2',
            'birthDay' => '20-01-2020'
        ];

        $form = $this->factory->create(AddressBookType::class);

        $form->submit($formData);
        $view = $form->createView();
        $this->assertFalse($form->isValid());
        $this->assertFalse($view->children['firstName']->vars['valid']);
    }

    public function testSubmitValidData()
    {
        $datetime = new \DateTime('2011-01-01');
        $date = $datetime->format(\DateTimeInterface::ISO8601);
        $formData = [
            'firstName' => 'Test',
            'lastName' => 'Test2',
            'address' => 'test',
            'city' => 'Test',
            'country' => 'DE',
            'zip' => '123456',
            'email' => 'test@gmail.com',
            'phone' => '1234567890',
            'birthDay' => $date
        ];

        $form = $this->factory->create(AddressBookType::class);

        $form->submit($formData);
        $this->assertTrue($form->isValid());
        $this->assertTrue($form->isSynchronized());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
